from __future__ import print_function
import Pyro4
import socket, fcntl, struct


class Warehouse(object):
    def __init__(self):
        self.contents = ["chair", "bike", "flashlight", "laptop", "couch"]

    def list_contents(self):
        return self.contents

    def take(self, name, item):
        self.contents.remove(item)
        print("{0} took the {1}.".format(name, item))

    def store(self, name, item):
        self.contents.append(item)
        print("{0} stored the {1}.".format(name, item))


def get_local_ip(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        addr = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
    except IOError:
        addr = None
    return addr


def main():
    warehouse = Warehouse()
    # get local IP
    addr = None
    addr = get_local_ip("wlan0")
    if not addr:
        addr = get_local_ip("eth0")

    Pyro4.Daemon.serveSimple(
        {
            warehouse: "s"

        },
        host=addr,
        port=1234,
        ns=False)


if __name__ == "__main__":

    main()
